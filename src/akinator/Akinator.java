package akinator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author benja
 */
public class Akinator extends Application {
    
    @Override
    public void start(Stage stage) throws Exception 
    {
        FXMLLoader vue          = new FXMLLoader();
        AnchorPane pane         = (AnchorPane) vue.load(getClass().getResourceAsStream("/akinator/views/AkinatorAccueil.fxml"));
        Scene scene             = new Scene(pane);
        stage.setScene(scene);
        stage.setTitle("Akinator - By Benjamin");
        stage.show();
    }
    public static void main(String[] args) 
    {
        launch(args);
    }   

}