package akinator.views;

import akinator.controllers.Controller;
import akinator.views.modeleDeVue.AlertDialog;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AkinatorAccueilController extends Controller implements Initializable {
    
    private FadeTransition      fadeIn;
    @FXML private AnchorPane    rootPane;
    @FXML private Pane          accueilPanel;
    @FXML private Pane          questionPanel;
    @FXML private Pane          questionAjouter;
    @FXML private Pane          solutionProposition;
    @FXML private Pane          questionsChoix;
    @FXML private ImageView     imageProposition;
    @FXML private ImageView     imageAccueil;
    @FXML private ImageView     imageQuestionPanel;
    @FXML private ImageView     notFoundQuestion;
    @FXML private ImageView     foundProposition;
    @FXML private Label         compteurQuestion;
    @FXML private Label         questionRepondre;
    @FXML private Label         labelResultProposition;
    @FXML private TextField     nomSerieText;
    @FXML private TextField     reponseSerieText;
    @FXML private TextField     questionSerieText;   
    
    public AkinatorAccueilController()
    {
        try
        {
            super.initialiserJeu();
        }
        catch(Exception e)
        {
            this.exit();
        }
    }
    @Override 
    public void initialize(URL location, ResourceBundle resources) 
    {
        this.imageAccueil.setImage(new Image(getClass().getResourceAsStream("/akinator/media/akinatorAccueil.png")));
        this.imageQuestionPanel.setImage(new Image(getClass().getResourceAsStream("/akinator/media/akinatorQuestion.png")));
        this.notFoundQuestion.setImage(new Image(getClass().getResourceAsStream("/akinator/media/akinatorNotFound.png")));
        this.foundProposition.setImage(new Image(getClass().getResourceAsStream("/akinator/media/akinatorProposition.png")));
        this.hidePanel();
        this.fadeIn = new FadeTransition(Duration.millis(1000));
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.setCycleCount(1);
        fadeIn.setAutoReverse(false);
        this.showPanel(this.accueilPanel);
    }  
    @FXML
    private void handleButtonActionStart(ActionEvent event) 
    {
        try 
        {
            super.miseAZero();
            this.initQuestion();
        } 
        catch (Exception ex) 
        {
            this.showMessage(ex.getMessage());
            this.exit();
        }
    }
    private void initQuestion() 
    {
        this.hidePanel();
        if(super.finDuJeuTrouve())
        {
            this.endGamesFind(); 
            return;
        }
        if(super.finDuJeuNonTrouve())
        {
            this.endGamesNoFind(); 
            return;
        }
        this.showQuestion();
    }
    private void endGamesFind()
    {
        this.imageProposition.setImage(new Image(getClass().getResourceAsStream("/akinator/media/series/"+super.getImageProposition())));
        this.labelResultProposition.setText(super.getNomProposition());
        this.showPanel(this.solutionProposition);
    }
    private void endGamesNoFind()
    {
        this.questionSerieText.setDisable(true);
        this.reponseSerieText.setDisable(true);
        this.showPanel(this.questionAjouter);
        this.showMessage("Je ne connais pas cette série, aidez-moi à me développer !");
    }
    private void showQuestion()
    {
        this.questionRepondre.setText(super.nextQuestion());
        this.compteurQuestion.setText("Question "+(super.getNumeroQuestion()+1));
        this.poserBoutonsReponses();
        this.showPanel(this.questionPanel);
    }
    private void poserBoutonsReponses()
    {
        this.questionsChoix.getChildren().clear();
        this.questionsChoix.getChildren().addAll(this.ajouterBoutons());
    }
    private List<Button> ajouterBoutons() 
    {
        try
        {
            Set<String> reponses = super.chargerReponse();
            return this.genererBoutons(reponses);
        }
        catch(Exception e)
        {
            this.showMessage("Une erreur fatale vient de survenir \n"+e.getMessage());
            this.exit();
        }
        return null;
    }
    private List<Button> genererBoutons(Set<String> reponses)
    {
        List<Button> boutons = new ArrayList<>();
        double positionY = 10.00;
        for (String reponse : reponses) 
        {
            Button btn = this.genererBouton(reponse, 220.0, 30.0, positionY, true);
            btn.setOnAction((ActionEvent event1) -> 
            {
                super.proposititionReponse(((Button)event1.getSource()).getText());
                this.initQuestion();
            });
            boutons.add(btn);
            positionY += 50;
        }
        Button btn = this.genererBouton("Cliquez ici pour ajouter une réponse", 220.0, 35.0, positionY, false);
        btn.setOnAction((ActionEvent event1) -> 
        {
            this.hidePanel();
            this.showPanel(this.questionAjouter);
            this.questionSerieText.setDisable(true);
            this.reponseSerieText.setDisable(false);
            this.showMessage("Une nouvelle série à encoder ? Vous êtes au bon endroit !");
        });
        boutons.add(btn);
        return boutons;
    }
    private Button genererBouton(String text, double sizeX, double sizeY, double positionY, boolean style)
    {
        Button bouton = new Button();
        bouton.setMinSize(sizeX, sizeY);
        bouton.setLayoutY(positionY);
        if(style)
            bouton.setStyle("-fx-background-color:\n" + "linear-gradient(#f0ff35, #a9ff00),\n" +   "radial-gradient(center 50% -40%, radius 200%, #b8ee36 45%, #80c800 50%);\n" +"-fx-background-radius: 6, 5;\n" +"-fx-background-insets: 0, 1;\n" +"-fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.4) , 5, 0.0 , 0 , 1 );\n" +"-fx-text-fill: #395306;-fx-font-size: 16px;");
        bouton.setText(text);
        return bouton;
    }
    @FXML
    private void handleButtonActionYesAjouter(ActionEvent event) 
    {
        try
        {
            if(!this.questionSerieText.isDisabled()) // Une question est ajoutée au jeu 
                super.ajouterSerie(nomSerieText.getText(), reponseSerieText.getText(), questionSerieText.getText());
            else if(!this.reponseSerieText.isDisabled()) // Une réponse est ajoutée au jeu
                super.ajouterSerie(nomSerieText.getText(), reponseSerieText.getText());
            else //Une simple série est ajoutée au jeu (lorsqu'il n'y avait pas de solution)
                super.ajouterSerie(this.nomSerieText.getText());
            this.showMessage("Merci pour votre collaboration !");
            this.hidePanel();
            this.showPanel(accueilPanel);
        }
        catch(Exception ex)
        {
            this.showMessage(ex.getMessage());
        }
    }
    @FXML
    private void handleButtonActionPropositionYes(ActionEvent event) 
    {
        this.showMessage("J'ai gagné ! Cette série a déjà été jouée "+super.totalJoue()+ " fois.\n\n Merci d'avoir joué !");
        this.hidePanel();
        this.showPanel(accueilPanel);
    }
    @FXML
    private void handleButtonActionPropositionNo(ActionEvent event) 
    {
        this.hidePanel();
        this.showPanel(this.questionAjouter);
        this.questionSerieText.setDisable(false);
        this.reponseSerieText.setDisable(false);
    }
    private void hidePanel()
    {
        this.accueilPanel.setVisible(false);
        this.questionPanel.setVisible(false);
        this.questionAjouter.setVisible(false);
        this.solutionProposition.setVisible(false);
    }
    private void showPanel(Pane panel)
    {
        fadeIn.setNode(panel);
        panel.setVisible(true);
        fadeIn.playFromStart();
    }
    @FXML
    private void handleButtonActioncClose(ActionEvent event) 
    {
        super.saveChangement();
        this.exit();
    }
    private void showMessage(String message)
    {
        new AlertDialog((Stage)rootPane.getScene().getWindow(), message).showAndWait();
    }
    private void exit()
    {
        Platform.exit();
        System.exit(0);
    }
    
}