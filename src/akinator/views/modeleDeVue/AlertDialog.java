package akinator.views.modeleDeVue;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Classe expérimentale permettant de pouvoir simuler une pop-up retiré depuis SWING
 * @author benja
 */
public class AlertDialog extends Stage {

    private final int WIDTH_DEFAULT = 300;
    
    public AlertDialog(Stage owner, String msg) 
    {
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        initStyle(StageStyle.TRANSPARENT);
        Label label = new Label(msg);
        label.setWrapText(true);
        label.setGraphicTextGap(20);
        Button button = new Button("Fermer la fenêtre");
        button.setOnAction(new EventHandler(){
            @Override
            public void handle(Event event) {
                AlertDialog.this.close();
            }
        });
        BorderPane borderPane = new BorderPane();
        borderPane.setStyle("-fx-background-color: #c0c0c0;\n"+"-fx-background-radius: 5;\n"+"-fx-padding: 10;");
        borderPane.setTop(label);
        HBox hbox2 = new HBox();
        hbox2.setAlignment(Pos.CENTER);
        hbox2.getChildren().add(button);
        borderPane.setBottom(hbox2);
        final Text text = new Text(msg);
        text.snapshot(null, null);
        int width = (int) text.getLayoutBounds().getWidth() + 40;
        if (WIDTH_DEFAULT <= width)
            width = WIDTH_DEFAULT;
        int height = 100;
        final Scene scene = new Scene(borderPane, width, height);
        scene.setFill(Color.TRANSPARENT);
        setScene(scene);
        setX(owner.getX() + (owner.getWidth() / 2 - width / 2));
        setY(owner.getY() + (owner.getHeight() / 2 - height / 2));
    }

}