package akinator.models;

import akinator.models.gestion.BanqueDeConnaissance;
import akinator.models.serie.Serie;
import akinator.models.serie.QuestionSerie;
import akinator.models.gestion.dataGame.DataAkinator;
import akinator.models.sound.Player;
import akinator.models.sound.Sons;
import akinator.models.sound.Sound;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Akinator {
    
    private final BanqueDeConnaissance data;
    private final Set<Serie> series;
    private final Map<QuestionSerie, String> cheminJeu;
    private final Player sonJeu;
    private QuestionSerie derniereQuestion;
    
    public Akinator() throws Exception 
    {
        this.data        = new BanqueDeConnaissance();
        this.series      = new LinkedHashSet<>();
        this.cheminJeu   = new LinkedHashMap<>();
        this.sonJeu      = new Sound();
        this.sonJeu.source(Sons.SON_READY, false);
    }
    /**
     * Méthode permettant d'intialiser le jeu, elle permettra également de relancer une partie
     */
    public void init()
    {
        if(this.derniereQuestion == null)
            this.sonJeu.source(Sons.SON_GAME, true);
        this.derniereQuestion = null;
        this.cheminJeu.clear();
    }
    /**
     * Méthode permettant de récupérer la prochaine question
     * @return Question a posée au joueur
     */
    public QuestionSerie nextQuestion()
    {
        if(this.derniereQuestion == null)
            this.derniereQuestion = this.data.getQuestion(0);
        else
            this.derniereQuestion = this.data.getQuestion(this.derniereQuestion.getId()+1);
        return this.derniereQuestion;
    }
    /**
     * Méthode permettant de charger une réponse
     * @return Réponses disponibles pour la dernière question posée
     * @throws Exception La question n'a pas été trouvée
     */
    public Set<String> chargerReponse() throws Exception
    {
        return this.data.getReponseQuestion(this.derniereQuestion);
    }
    /**
     * Méthode permettant de gérer la réponse proposée par le joueur
     * @param reponse La réponse choisie par le joueur
     */
    public void proposerReponse(String reponse) 
    {
        try
        {
            if(this.derniereQuestion.getId() == 0)
            {
                this.series.clear();
                this.series.addAll(this.data.getSerieQuestion(derniereQuestion, reponse));
            }
            this.cheminJeu.put(derniereQuestion, reponse);
            this.series.retainAll(this.data.getSerieQuestion(derniereQuestion, reponse));
        }
        catch(Exception ex){}
    }
    /**
     * Méthode vérifiant s'il ne reste qu'une seule série et donc que le jeu est terminé
     * @return 
     */
    public boolean finDuJeuTrouve()
    {
        return this.derniereQuestion != null && this.series.size() == 1;
    }
    /**
     * Méthode vérifiant s'il ne reste plus aucune série à proposer et donc que le jeu est terminé
     * @return 
     */
    public boolean finDuJeuNonTrouve()
    {
        return this.derniereQuestion != null && this.series.isEmpty();
    }
    /**
     * Méthode renvoyant la dernière question
     * @return 
     */
    public QuestionSerie lastQuestion()
    {
        return this.derniereQuestion;
    }
    /**
     * Méthode renvoyant le nom du personnage trouvé
     * @return 
     */
    public String getNomPersonnageTrouve()
    {
        return this.series.iterator().next().toString();
    }
    /**
     * Méthode renvoyant l'image du personnage trouvé
     * @return 
     */
    public String getImagePersonnageTrouve() 
    {
        return this.series.iterator().next().getImage();
    }
    /**
     * Méthode renvoyant le numéro de la dernière question posée
     * @return 
     */
    public int getNumeroLastQuestion() 
    {
        return this.derniereQuestion.getId();
    }
    /**
     * Méthode permettant d'ajouter une série liée au chemin du jeu effectué
     * @param nomSerie Nom de la série
     * @throws Exception Information incorrect
     */
    public void ajouterSerie(String nomSerie) throws Exception
    {
        if(nomSerie == null || nomSerie.equals(""))
            throw new Exception("Veuillez compléter le nom de la série.");
        Serie serie = new Serie();
        serie.setNom(nomSerie);
        serie.setId(this.data.getDataSeriesSize());
        this.cheminJeu.forEach((QuestionSerie question, String reponse) -> 
        {
            this.data.addAnswer(question, reponse, serie);  
        });
    }
    /**
     * Méthode permettant d'ajouter une série liée à une question
     * @param nomSerie Nom de la série
     * @param reponse Réponse à la question (vis à vis de la dernière question)
     * @throws Exception Ajout impossible car informations incorrectes
     */
    public void ajouterSerie(String nomSerie, String reponse) throws Exception
    {
        if(reponse == null || reponse.equals(""))
            throw new Exception("Veuillez compléter le nom de la série et sa réponse.");
        this.ajouterSerie(nomSerie);
        Serie serie = new Serie();
        serie.setNom(nomSerie);
        serie.setId(this.data.getDataSeriesSize());
        this.data.addAnswer(derniereQuestion, reponse, serie);   
    }
    /**
     * Méthode permettant d'ajouter une série liée à une quetion et sa réponse
     * @param nomSerie Nom de la série
     * @param reponse Réponse à la question
     * @param question Nouvelle question
     * @throws Exception Ajout impossible car informations incorrectes
     */
    public void ajouterSerie(String nomSerie, String reponse, String question) throws Exception
    {
        if(reponse == null || reponse.equals("") || question == null || question.equals(""))
            throw new Exception("Veuillez compléter les informations nécessaires.");
        Serie serie = new Serie();
        serie.setNom(nomSerie);
        serie.setId(this.data.getDataSeries().size());
        this.cheminJeu.forEach((QuestionSerie laQuestion, String laReponse) -> 
        {
            this.data.addAnswer(laQuestion, laReponse, serie);  
        });
        QuestionSerie nouvelleQuestion = new QuestionSerie(question);
        nouvelleQuestion.setId(this.data.getTailleQuestion());
        this.data.addAnswer(nouvelleQuestion, reponse, serie);
    }
    /**
     * Méthode permettant de sauver les changements effectués
     */
    public void saveChangement()
    {
        DataAkinator save = new DataAkinator();
        save.writeFile(this.data);
    }
    /**
     * Méthode renvoyant le total de parties jouées sur la série trouvée et validée
     * @return 
     */
    public int totalJoueSolution() 
    {
        if(this.series == null || this.series.toArray().length != 1)
            return 0;
        Serie a = ((Serie)this.series.toArray()[0]);
        a.setNbJoues(a.getNbJoues()+1);
        return a.getNbJoues();
    }
    
}