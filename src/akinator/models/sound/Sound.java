package akinator.models.sound;

import java.net.URISyntaxException;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Sound implements Player {
    
    private MediaPlayer mediaPlayer;
    
    private void initPlayer(String music, boolean repeat)
    {
        if(mediaPlayer != null)
            this.stop();
        try 
        {
            Media sound     = new Media(getClass().getResource("/akinator/media/sound/"+music).toURI().toString());
            this.mediaPlayer     = new MediaPlayer(sound);
            if(repeat)
                 this.mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
            this.play();
        } 
        catch (URISyntaxException ex) {}
    }
    @Override
    public void source(String music, boolean repeat)
    {
        if(music == null)
            return;
        this.initPlayer(music, repeat);
    }
    @Override
    public void stop()
    {
        if(this.mediaPlayer.getStatus() == MediaPlayer.Status.PLAYING)
            mediaPlayer.stop();
    }
    @Override
    public void play() 
    {
        this.mediaPlayer.play();
    }
    @Override
    public void pause() 
    {
        this.mediaPlayer.pause();
    }

}