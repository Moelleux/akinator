package akinator.models.sound;

/**
 * Interface générale concernant les player
 * @author benja
 */
public interface Player {
    
    void play();
    void stop();
    void pause();
    void source(String source, boolean repeat);

}