package akinator.models.serie;

import java.util.Objects;

public class QuestionSerie {
    
    private int id;
    private String question;
    
    public QuestionSerie(String question)
    {
        this.question = question;
    }
    public QuestionSerie(int id, String question) 
    {
        this.question           = question;
        this.id                 = id;
    }
    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if (obj == this) 
            return true;
        if (getClass() == obj.getClass()) 
        {
            final QuestionSerie other = (QuestionSerie) obj;
            return Objects.equals(this.question, other.question);
        }
        return false;
    }
    @Override
    public int hashCode() 
    {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }
    @Override
    public String toString()
    {
        return this.question;
    }
    public void setId(int id) { this.id = id; }
    public int getId() { return id; }
    public String getQuestion() { return question; }
    public void setQuestion(String question) { this.question = question; }

}