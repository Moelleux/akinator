package akinator.models.serie;

import java.util.Objects;

public class Serie {

    private int id;
    private String nom;
    private int nbJoues;
    private String image;

    public Serie() 
    {
        this.nom        = "NON TROUVE";
        this.image      = "notFound.jpg";
    }
    public Serie(int id, String nom, int nbJoues, String image) 
    {
        this.id         = id;
        this.nom        = nom;
        this.nbJoues    = nbJoues;
        this.image      = image;
    }
    @Override
    public boolean equals(Object obj) 
    {
        if(obj == null)
            return false;
        if (obj == this) 
            return true;
        if (getClass() == obj.getClass()) 
        {
            final Serie other = (Serie) obj;
            return Objects.equals(this.nom, other.nom);
        }
        return false;
    }
    @Override
    public int hashCode() 
    {
        int hash = 3;
        hash = 23 * hash + this.id;
        return hash;
    }
    @Override
    public String toString()
    {
        return this.nom;
    }
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public int getNbJoues() { return nbJoues; }
    public void setNom(String nom) { this.nom = nom; }
    public void setNbJoues(int nbJoues) { this.nbJoues = nbJoues; }
    public void setImage(String image) { this.image = image; }
    public String getImage() { return this.image; }
  
}