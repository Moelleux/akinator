package akinator.models.gestion;

import akinator.models.gestion.dataGame.Data;
import akinator.models.gestion.dataGame.DataQuestions;
import akinator.models.gestion.dataGame.DataReponses;
import akinator.models.gestion.dataGame.DataSeries;
import akinator.models.serie.QuestionSerie;
import akinator.models.serie.Serie;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BanqueDeConnaissance {

    private final Map<QuestionSerie, Map<String, Set<Serie>>> data;
    private final List<Serie> dataSeries;
    
    public BanqueDeConnaissance() throws Exception 
    {
        data        = new LinkedHashMap<>();
        dataSeries  = new ArrayList<>();
        this.initData();
    }
    /**
     * Méthode initialisant la banque de donnée (il charge les questions, les séries  et effectue les liens)
     * @throws Exception Lecture impossible
     */
    private void initData() throws Exception
    {
        Data donnees                    = new DataQuestions();
        List<QuestionSerie> questions   = donnees.lireContenu();
        donnees                         = new DataSeries();
        List<Serie> series              = donnees.lireContenu();
        this.setDataSeries(series);
        donnees                         = new DataReponses();
        List<String> reponses           = donnees.lireContenu();
        reponses.forEach((String current) -> 
        { 
            String[] valeurs = current.split(";");
            this.addQuestion(questions.get(Integer.parseInt(valeurs[0].trim())));
            for(int i = 2; i < valeurs.length; i++)
                this.addAnswer(questions.get(Integer.parseInt(valeurs[0])), valeurs[1].trim(), series.get(Integer.parseInt(valeurs[i].trim())));
        });
    }
    /**
     * Méthodes permettant de poser les séries du jeu
     * @param series Liste des séries
     */
    public void setDataSeries(List<Serie> series)
    {
        this.dataSeries.clear();
        this.dataSeries.addAll(series);
    }
    /**
     * Méthode retournant le nombre de séries
     * @return 
     */
    public int getDataSeriesSize()
    {
        return this.dataSeries.size();
    }
    /**
     * Méthode retourner la liste des séries
     * @return 
     */
    public List<Serie> getDataSeries()
    {
        return this.dataSeries;
    }
    /**
     * Méthode permettant d'ajouter une question
     * @param question Question à ajouter
     */
    public void addQuestion(QuestionSerie question) 
    {
        if (!data.containsKey(question)) 
        {
            data.put(question, new HashMap<>());
        }
    }
    /**
     * Méthode permettant d'ajouter une liste de question
     * @param questions Liste des questions
     */
    public void addQuestionAll(List<QuestionSerie> questions) 
    {
        questions.stream().filter((question) -> (!data.containsKey(question))).forEach((question) -> 
        {
            data.put(question, new HashMap<>());
        });
    }
    /**
     * Méthode retournant la liste des questions
     * @return 
     */
    public Set<QuestionSerie> getQuestion()
    {
        return this.data.keySet();
    }
    /**
     * Méthode retournant une question selon son indice
     * @param indice indice de la question
     * @return Question
     */
    public QuestionSerie getQuestion(int indice)
    {
        return (QuestionSerie)this.data.keySet().toArray()[indice];
    }
    /**
     * Retourne le nombre total de questions
     * @return 
     */
    public int getTailleQuestion()
    {
        return this.data.size();
    }
    /**
     * Méthode permettant de récupérer les questions d'une question
     * @param question Question posée
     * @return Liste des réponses possibles
     * @throws Exception La question n'existe pas
     */
    public Set<String> getReponseQuestion(QuestionSerie question) throws Exception 
    {
        if (!data.containsKey(question)) 
        {
            throw new Exception("La question n'existe pas.");
        }
        return data.get(question).keySet();
    }
    /**
     * Méthode permettant d'ajouter une réponse selon une question et une série
     * @param question question 
     * @param reponse réponse de la question
     * @param serie Série
     */
    public void addAnswer(QuestionSerie question, String reponse, Serie serie) 
    {
        if(!data.containsKey(question))
            this.addQuestion(question);
        if (data.get(question).containsKey(reponse)) 
            data.get(question).get(reponse).add(serie);
        else 
        {
            if(serie.getId() == this.getDataSeriesSize())
                serie.setId(this.dataSeries.size()-1);
            Set series = new HashSet<>();
            series.add(serie);
            data.get(question).put(reponse, series);
        }
        if(!this.dataSeries.contains(serie))
            this.dataSeries.add(serie);
    }
    /**
     * Méthode permettant de récupérer la liste des séries posssible selon une question et sa réponse
     * @param question
     * @param reponse
     * @return
     * @throws Exception 
     */
    public Set<Serie> getSerieQuestion(QuestionSerie question, String reponse) throws Exception 
    {
        if (!data.containsKey(question)) 
        {
            throw new Exception("La quetion n'existe pas.");
        } 
        else if (!data.get(question).containsKey(reponse)) 
        {
            throw new Exception("La réponse n'existe pas pour cette question.");
        }
        return data.get(question).get(reponse);
    }
    /**
     * Méthode vérifiant s'il existe encore des questions possibles selon une liste de series
     * @param derniereQuestion
     * @param series
     * @return Vrai s'il reste des questions
     */
    public boolean getExistsSerieQuestion(QuestionSerie derniereQuestion, Set<Serie> series) 
    {
        if (!data.containsKey(derniereQuestion)) 
        {
            return false;
        } 
        Set<Serie> current = new HashSet();
        this.data.get(derniereQuestion).forEach((String k, Set<Serie> v) -> 
        {
            current.addAll(v);
        });
        current.retainAll(series);
        return !current.isEmpty();
    }
  
}