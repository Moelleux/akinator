package akinator.models.gestion;

public class ConfigDatabase {
 
    public static final String CHEMIN_QUESTIONS = "questions.csv";
    public static final String CHEMIN_REPONSES  = "reponses.csv";
    public static final String CHEMIN_SERIES    = "series.csv";
    
}