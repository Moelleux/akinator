package akinator.models.gestion.fichier;

import java.util.List;

public interface DataFichier {
    
    /**
     * Méthode permettant de lire un fichier de type texte
     * @param path chemin du fichier
     * @return contenu du fichier
     * @throws Exception ERREUR DE TRAITEMENT
     */
    List<String> readFile(String path) throws Exception;
    /**
     * Méthode permettant d'écrire un fichier
     * @param contenu contenu du fichier
     * @param path chemin du fichier
     * @throws Exception ERREUR DE TRAITEMENT
     */
    void writeFile(String contenu, String path) throws Exception;
    
}