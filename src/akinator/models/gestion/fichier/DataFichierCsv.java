package akinator.models.gestion.fichier;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DataFichierCsv implements DataFichier {
 
    @Override
    public List<String> readFile(String path) throws Exception 
    {
        return Files.readAllLines(Paths.get(path), Charset.forName("UTF-8"));
    }
    @Override
    public void writeFile(String contenu, String path) throws Exception
    {
        File file = new File(path);
        if(!file.exists()) 
        {
            try { file.createNewFile(); } 
            catch(IOException ex) {}
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path, false))) 
        {
            writer.write(contenu);
            writer.close();
        } 
        catch (Exception ex) 
        { 
            throw new Exception("ECRITURE IMPOSSIBLE");
        }
    }
    
}