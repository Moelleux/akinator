package akinator.models.gestion.dataGame;

import akinator.models.gestion.ConfigDatabase;
import akinator.models.gestion.fichier.DataFichier;
import akinator.models.gestion.fichier.DataFichierCsv;
import akinator.models.serie.QuestionSerie;
import java.util.ArrayList;
import java.util.List;

public class DataQuestions implements Data<QuestionSerie> {
 
    /**
     * Méthode permettant de lire le contenu des questions
     * @return Liste des questions
     * @throws Exception ERREUR DE TRAITEMENT FICHIER
     */
    @Override
    public List<QuestionSerie> lireContenu() throws Exception
    {
        List<QuestionSerie> questions   = new ArrayList<>();
        DataFichier data                = new DataFichierCsv();
        List<String> contenu            = data.readFile(ConfigDatabase.CHEMIN_QUESTIONS);
        contenu.forEach((String current) -> 
        { 
            String[] donnees = current.split(";");
            questions.add(new QuestionSerie(Integer.parseInt(donnees[0]), donnees[1])); 
        });
        return questions; 
    }

}