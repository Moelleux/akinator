package akinator.models.gestion.dataGame;

import java.util.List;

/**
 * Interface permettant de lire les différents contenu (question / réponses / séries)
 * @author benja
 * @param <T> Type d'objet à lire
 */
public interface Data<T> {

    List<T> lireContenu() throws Exception;

}