package akinator.models.gestion.dataGame;

import akinator.models.gestion.BanqueDeConnaissance;
import akinator.models.gestion.ConfigDatabase;
import akinator.models.gestion.fichier.DataFichier;
import akinator.models.gestion.fichier.DataFichierCsv;
import akinator.models.serie.QuestionSerie;
import akinator.models.serie.Serie;
import java.util.List;
import java.util.Set;

public class DataAkinator {

    /**
     * Méthode expérimentale (mais qui fonctionne) permettant d'écrire dans les fichiers CSV selon la banque de connaissance
     * @param data banque de connaissance
     */
    public void writeFile(BanqueDeConnaissance data)
    {
        try 
        {
            this.writeSerie(data.getDataSeries());
            this.writeQuestion(data.getQuestion());
            StringBuilder builder  = new StringBuilder();
            for(QuestionSerie question : data.getQuestion()) 
            {
                for (String reponse : data.getReponseQuestion(question)) 
                {
                    builder.append(question.getId());
                    builder.append(';');
                    builder.append(reponse);
                    builder.append(';');
                    for (Serie serie : data.getSerieQuestion(question, reponse)) 
                    {
                        builder.append(serie.getId());
                        builder.append(';');
                    }
                    builder.deleteCharAt(builder.length() - 1);
                    builder.append('\n');
                }
            }
            builder.deleteCharAt(builder.length() - 1);
            DataFichier donnees = new DataFichierCsv();
            donnees.writeFile(builder.toString(), ConfigDatabase.CHEMIN_REPONSES);
        } 
        catch (Exception ex) {}
    }
    /**
     * Méthode permettant d'écrire les différentes séries dans un fichier CSV
     * @param series Les séries à enregistrer
     * @throws Exception ERREUR DE TRAITEMENT
     */
    public void writeSerie(List<Serie> series) throws Exception
    {
        StringBuilder builder  = new StringBuilder();
        series.stream().map((serie) -> {
            builder.append(serie.getId());
            return serie;
        }).map((serie) -> {
            builder.append(';');
            builder.append(serie.toString());
            return serie;
        }).map((serie) -> {
            builder.append(';');
            builder.append(serie.getNbJoues());
            return serie;
        }).map((serie) -> {
            builder.append(';');
            builder.append(serie.getImage());
            return serie;
        }).forEach((_item) -> {
            builder.append('\n');
        });
        builder.deleteCharAt(builder.length() - 1);
        DataFichier donnees = new DataFichierCsv();
        donnees.writeFile(builder.toString(), ConfigDatabase.CHEMIN_SERIES);
    }
    /**
     * Méthode permettant d'écrire les questions dans un fichier texte
     * @param series séries à écrire
     * @throws Exception ERREUR DE TRAITEMENT
     */
    public void writeQuestion(Set<QuestionSerie> series) throws Exception
    {
        StringBuilder builder  = new StringBuilder();
        series.stream().map((serie) -> {
            builder.append(serie.getId());
            return serie;
        }).map((serie) -> {
            builder.append(';');
            builder.append(serie.getQuestion());
            return serie;
        }).forEach((_item) -> {
            builder.append('\n');
        });
        builder.deleteCharAt(builder.length() - 1);
        DataFichier donnees = new DataFichierCsv();
        donnees.writeFile(builder.toString(), ConfigDatabase.CHEMIN_QUESTIONS);
    }
      
}