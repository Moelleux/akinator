package akinator.models.gestion.dataGame;

import akinator.models.gestion.ConfigDatabase;
import akinator.models.gestion.fichier.DataFichier;
import akinator.models.gestion.fichier.DataFichierCsv;
import java.util.List;

public class DataReponses implements Data<String> {

    /**
     * Méthode permettant de lire le contenu des réponses
     * @return Liste des réponses
     * @throws Exception ERREUR DE TRAITEMENT FICHIER
     */
    @Override
    public List<String> lireContenu() throws Exception
    {
        DataFichier data       = new DataFichierCsv();
        return data.readFile(ConfigDatabase.CHEMIN_REPONSES);
    }

}
