package akinator.models.gestion.dataGame;

import akinator.models.gestion.ConfigDatabase;
import akinator.models.gestion.fichier.DataFichier;
import akinator.models.gestion.fichier.DataFichierCsv;
import akinator.models.serie.Serie;
import java.util.ArrayList;
import java.util.List;

public class DataSeries implements Data<Serie> {

    /**
     * Méthode permettant de lire le contenu des séries
     * @return Liste des séries
     * @throws Exception ERREUR DE TRAITEMENT FICHIER
     */
    @Override
    public List<Serie> lireContenu() throws Exception 
    {
        List<Serie> result     = new ArrayList<>();
        DataFichier data       = new DataFichierCsv();
        List<String> contenu   = data.readFile(ConfigDatabase.CHEMIN_SERIES);
        contenu.forEach((String current) -> 
        { 
            String[] donnees = current.split(";");
            result.add(new Serie(Integer.parseInt(donnees[0].trim()), donnees[1].trim(), Integer.parseInt(donnees[2].trim()), donnees[3].trim())); 
        });
        return result;
    }
    
}