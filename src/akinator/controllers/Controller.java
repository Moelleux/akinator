package akinator.controllers;

import akinator.models.Akinator;
import java.util.Set;

public abstract class Controller {
 
    private Akinator game;
    /**
     * Initialise le jeu de base et ainsi indirectement la banque de conaissance
     * @throws Exception La banque de connaissance n'a pas su correctement se charger
     */
    public void initialiserJeu() throws Exception
    {
        this.game = new Akinator();
    }
    /**
     * Méthode permettant de remettre le jeu à zéro lors d'un relancement de partie
     */
    public void miseAZero()
    {
        this.game.init();
    }
    /**
     * Va chercher la prochaine question à poser
     * @return Le texte de la question
     */
    public String nextQuestion()
    {
        return this.game.nextQuestion().toString();
    }
    /**
     * Méthode permettant de charger les réponses de la question
     * @return  Collections de textes contenant les différentes réponses
     * @throws Exception La question est inexistante
     */
    public Set<String> chargerReponse() throws Exception
    {
        return this.game.chargerReponse();
    }
    /**
     * Méthode permettant au joueur de proposer une réponse à la question
     * @param reponse Reponse proposée par le joueur
     */
    public void proposititionReponse(String reponse) 
    {
        this.game.proposerReponse(reponse);       
    }
    /**
     * Méthode permettant de savoir si le jeu est terminé ET la réponse est trouvée
     * @return boolean si la réponse à été trouvée
     */
    public boolean finDuJeuTrouve()
    {
        return this.game.finDuJeuTrouve();
    }
    /**
     * Méthode permettant de savoir si le jeu est teminée ET la réponse n'a pas été trouvée (mais plus de série jouables)
     * @return boolean si le jeu est terminé et série non trouvée
     */
    public boolean finDuJeuNonTrouve()
    {
        return this.game.finDuJeuNonTrouve();
    }
    /**
     * Méthode retournant le nom de a proposition
     * @return 
     */
    public String getNomProposition() 
    {
        return this.game.getNomPersonnageTrouve();
    }
    /**
     * Méthode retournant l'image de la proposition
     * @return 
     */
    public String getImageProposition() 
    {
        return this.game.getImagePersonnageTrouve();
    }
     /**
     * Méthode retournant le total de fois que la série courante a été jouée
     * @return 
     */
    public int totalJoue() 
    {
        return this.game.totalJoueSolution();
    }
    /**
     * Méthode retournant le numéro de la question courant
     * @return 
     */
    public int getNumeroQuestion() 
    {
        return this.game.getNumeroLastQuestion()+1;
    }
    /**
     * Méthode permettant d'ajouter une série
     * @param nomSerie Nom de la série à ajouter
     * @throws Exception Ajout impossible car information incorrecte
     */
    public void ajouterSerie(String nomSerie) throws Exception 
    {
        this.game.ajouterSerie(nomSerie);
    }
    /**
     * Méthode permettant d'ajouter une série liée à une question
     * @param nomSerie Nom de la série
     * @param reponseQuestion Réponse à la question (vis à vis de la dernière question)
     * @throws Exception Ajout impossible car informations incorrectes
     */
    public void ajouterSerie(String nomSerie, String reponseQuestion) throws Exception
    {
        this.game.ajouterSerie(nomSerie, reponseQuestion);
    }
    /**
     * Méthode permettant d'ajouter une série liée à une quetion et sa réponse
     * @param nomSerie Nom de la série
     * @param reponseQuestion Réponse à la question
     * @param question Nouvelle question
     * @throws Exception Ajout impossible car informations incorrectes
     */
    public void ajouterSerie(String nomSerie, String reponseQuestion, String question) throws Exception 
    {
        this.game.ajouterSerie(nomSerie, reponseQuestion, question);
    }
    /**
     * Méthode permettant de sauver les changements effectués
     */
    public void saveChangement()
    {
        this.game.saveChangement();
    }
    
}